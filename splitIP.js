const dataSet = require('./p2.js');

const splitIP = data =>{
    let ipArr = data.map(element =>{
        const splittedIp = (element.ip_address.split("."));
        return splittedIp
    })

    //console.log(ipArr);
    return ipArr;
}

splitIP(dataSet);

module.exports = splitIP ;