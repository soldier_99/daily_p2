// Calculate how many .org, .au, .com emails are there

const dataSet = require('./p2.js');

const numOfMails = data =>{
    
    const numMails = data.reduce((accumulator, entry)=>{
        if(entry.email.includes('.org')){
            if(!accumulator['org']){
                accumulator.org = 1;
            }
            else{
                accumulator.org++;
            }
        }
        if(entry.email.includes('.au')){
            if(!accumulator['au']){
                accumulator.au = 1;
            }else{
                accumulator.au ++ ;
            }
        }
        if(entry.email.includes('.com')){
            if(!accumulator['com']){
                accumulator.com = 1;
            }else{
                accumulator.com ++;
            }
        }
        return accumulator ;
    },{})

    console.log(numMails);

}

numOfMails(dataSet);